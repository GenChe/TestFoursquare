//
//  ErrorCodes.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation

struct ErrorCodes {
    static let domain = "com.testfoursquare"
    static let mappingFailed = -1
    static let emptyLocation = -2
    static let locationServiceUnavailable = -3
    static let serviceUnavailable = -4
    static let unknownIssue = -5
}