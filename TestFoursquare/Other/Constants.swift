//
//  Constants.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation

struct Constants {
    static let accessToken = ""
    static let baseURL = "https://api.foursquare.com/v2/"
    static let appReleaseDate = "20151104"
}