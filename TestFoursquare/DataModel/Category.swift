//
//  Category.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 05.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import ObjectMapper

class Category {
    
    var name : String = ""
    var iconPrefix = ""
    var iconSuffix = ""
    
    required init?(_ map: Map) {
        debugPrint("start init categories")
    }
}

extension Category : Mappable {
    
    func mapping(map: Map) {
        name <- map["name"]
        iconPrefix <- map["icon.prefix"]
        iconSuffix <- map["icon.suffix"]
    }
}
