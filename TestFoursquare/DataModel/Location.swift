//
//  Location.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 06.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import ObjectMapper

class Location {
    
    var address = ""
    var city = ""
    var state = ""
    var country = ""
    var distance = 0
    var latitude = Double()
    var longitude = Double()
    
    required init?(_ map: Map) {
        debugPrint("start init venues")
    }
}

extension Location : Mappable {
    
    func mapping(map: Map) {
        address <- map["address"]
        city <- map["city"]
        state <- map["state"]
        country <- map["country"]
        distance <- map["distance"]
        latitude <- map["lat"]
        longitude <- map["lng"]
    }
}