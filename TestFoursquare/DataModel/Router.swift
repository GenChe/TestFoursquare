//
//  Router.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import CoreLocation

enum ListOption : String {
    case None = ""
    case Search = "/search"
}

enum Router<T:Mappable> : URLRequestConvertible {
    
    case GetList(location: CLLocation, limit: Int, radius : Int, listOption : ListOption)
    
    var method: Alamofire.Method {
        switch self {
        case .GetList:
            return .GET
        }
    }
    
    var path: String {
        switch self {
        case .GetList(_, _, _, let listOption):
            return String(T.self).lowercaseString + "s" + listOption.rawValue
        }
    }
    
    // MARK: URLRequestConvertible
    
    var URLRequest: NSMutableURLRequest {
        let mutableURLRequest = NSMutableURLRequest(URL: NSURL(string: Constants.baseURL + path)!)
        mutableURLRequest.HTTPMethod = method.rawValue
        
        switch self {
        case .GetList(let location, let limit, let radius, _):
            let v = String(String(location.coordinate.latitude) + "," + String(location.coordinate.longitude))
            
            return Alamofire.ParameterEncoding.URL.encode(mutableURLRequest, parameters:[
                "ll"            : v,
                "limit"         : limit,
                "radius"        : radius,
                "v"             : Constants.appReleaseDate,
                "oauth_token"   : Constants.accessToken]).0
        }
    }
}