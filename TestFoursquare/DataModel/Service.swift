//
//  Service.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import BrightFutures
import ObjectMapper
import AlamofireObjectMapper
import Alamofire
import SwiftLocation
import CoreLocation

let defaultLocation = 0
let defaultLimit = 10
let defaultRadius = 1000

class Service<T: Mappable> {
    
    class func getList(location: Int = defaultLocation, limit: Int = defaultLimit, radius : Int = defaultRadius,
        listOption: ListOption = ListOption.None) -> Future<[T], NSError> {
            
            let promise = Promise<[T], NSError>()
            
            do {
                try SwiftLocation.shared.currentLocation(.Neighborhood, timeout: 10, onSuccess: { currentLocation in
                    
                    guard let loc = currentLocation else {
                        return promise.failure(NSError(domain: ErrorCodes.domain, code: ErrorCodes.emptyLocation, userInfo: nil))
                    }
                    
                    _getList(loc, limit: limit, radius: radius, listOption: listOption, promise: promise)
                    
                    }) { error in
                        promise.failure(error ?? NSError(domain: ErrorCodes.domain, code: ErrorCodes.emptyLocation, userInfo: nil))
                }
            } catch {
                promise.failure(NSError(domain: ErrorCodes.domain, code: ErrorCodes.unknownIssue, userInfo: nil))
            }
            
            return promise.future
            
    }
    
    private class func _getList(location: CLLocation, limit: Int, radius : Int, listOption: ListOption, promise : Promise<[T], NSError>) {
        
        Alamofire.request(
            Router<T>.GetList(location: location, limit: limit, radius: radius, listOption: listOption))
            .validate()
            .responseObject { (response : Response<ServerResponse<T> , NSError>) in
                
                guard response.result.error == nil else {
                    return promise.failure(response.result.error!)
                }
                guard let venues = response.result.value?.venues else {
                    return promise.failure(NSError(domain: ErrorCodes.domain,
                        code: ErrorCodes.mappingFailed, userInfo: nil))
                }
                promise.success(venues)
        }
        
    }
    
}