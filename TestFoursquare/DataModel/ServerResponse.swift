//
//  Response.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 05.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import ObjectMapper

class ServerResponse<T: Mappable> {
    
    var venues = [T]()
    
    required init?(_ map: Map) {
        debugPrint("start init response")
    }
}

extension ServerResponse : Mappable {
    
    func mapping(map: Map) {
        venues <- map["response.venues"]
    }
}