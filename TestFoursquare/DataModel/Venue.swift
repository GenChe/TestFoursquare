//
//  Venue.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import ObjectMapper

class Venue {
    
    var name = ""
    var location : Location!
    var categories = [Category]()
    
    required init?(_ map: Map) {
        debugPrint("start init venues")
    }
}

extension Venue : Mappable {
    
    func mapping(map: Map) {
        name <- map["name"]
        location <- map["location"]
        categories <- map["categories"]
    }
}
