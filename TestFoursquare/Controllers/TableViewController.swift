//
//  TableViewController.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import UIKit
import PermissionScope

class TableViewController: UITableViewController {
    
    let showDetailedVenue = "showDetailedVenue"
    var venues = [Venue]()
    let cellID = "defaultCellID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let permissionScope = PermissionScope()
        switch permissionScope.statusLocationInUse() {
            
        case .Disabled, .Unauthorized, .Unknown:
            
            permissionScope.addPermission(LocationWhileInUsePermission(),
                message: "We use this to track\r\nwhere you locate")
            permissionScope.show({ (finished, results) in
                
                guard results[0].status == .Authorized || results[0].status == .Unknown else {
                    return self.showAlert("Oops", message: "We can't do anything for you without permission")
                }
                self.refreshAction()
                }) { _ in
                    self.showAlert("Oops", message: "We can't do anything for you without permission")
            }
            return
        case .Authorized:
            return refreshAction()
        }
    }
    
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Cancel, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func updatedListRecieved(list: [Venue]) {
        venues = list
        
        dispatch_async(dispatch_get_main_queue()) {
            self.tableView.reloadData()
        }
    }
    
    func loadVenues() {
        if refreshControl != nil && !refreshControl!.refreshing {
            refreshControl!.beginRefreshing()
        }
        Service<Venue>.getList(listOption: .Search)
            .onSuccess { [weak self] list in
                guard let sSelf = self else { return }
                sSelf.updatedListRecieved(list)
            }
            .onFailure { [weak self] error in
                debugPrint(error)
                self?.showAlert(error.domain, message: String(error.code))
            }
            .onComplete { [weak self] _ in
                guard let sSelf = self else {return}
                sSelf.refreshControl?.endRefreshing()
        }
    }
    
    @IBAction func refreshAction() {
        loadVenues()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == showDetailedVenue {
            guard let venue = sender as? Venue else {return}
            guard let vc = segue.destinationViewController as? DetailedVenueViewController else {return}
            vc.venue = venue
        }
    }
}

extension TableViewController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return venues.count
    }
    
    override  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCellWithIdentifier(cellID, forIndexPath: indexPath) as? TableViewCell)
            ?? TableViewCell()
        cell.apply(venues[indexPath.row])
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier(showDetailedVenue, sender: venues[indexPath.row])
    }
}
