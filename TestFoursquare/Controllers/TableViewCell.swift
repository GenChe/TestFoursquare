//
//  TableViewCell.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 04.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var venueTitle: UILabel!
    @IBOutlet weak var mainCategory: UILabel!
    @IBOutlet weak var distanceToVenue: UILabel!
    
    func apply(venue: Venue) {
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
            self?.venueTitle.text = venue.name
            self?.mainCategory.text = venue.categories.first?.name
            self?.distanceToVenue.text = String(venue.location.distance)
        }
    }
}
