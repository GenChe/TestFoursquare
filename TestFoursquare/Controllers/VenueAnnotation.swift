//
//  VenueAnnotation.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 05.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import Foundation
import MapKit

class VenueAnnotation: NSObject, MKAnnotation {
    
    let venue: Venue
    let title: String?
    let subtitle: String?
    let coordinate: CLLocationCoordinate2D
    
    init(venue: Venue) {
        self.coordinate = CLLocationCoordinate2D(latitude: venue.location.latitude, longitude: venue.location.longitude)
        self.subtitle = venue.categories.first?.name
        self.title = venue.name
        self.venue = venue
        super.init()
    }
}
