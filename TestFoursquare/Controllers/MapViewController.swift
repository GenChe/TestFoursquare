//
//  MapViewController.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 05.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    let regionRadius: CLLocationDistance = 1000
    @IBOutlet var mapView: MKMapView!
    var venue : Venue?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        guard let unwVenue = venue else {return}
        centerMapOnLocation(CLLocation(latitude: unwVenue.location.latitude, longitude: unwVenue.location.longitude))
        mapView.addAnnotation(VenueAnnotation(venue: unwVenue))
    }
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
}
