//
//  DetailedVenueViewController.swift
//  TestFoursquare
//
//  Created by Eugene Khruscheff on 05.11.15.
//  Copyright © 2015 Eugene Khruscheff. All rights reserved.
//

import UIKit
import SDWebImage

class DetailedVenueViewController : UIViewController {
    
    let showOnMap = "showOnMap"
    var venue : Venue?
    
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var venueName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: true)
        
        guard let unwVenue = venue else {return}
        
        venueName.text = unwVenue.name
        country.text = unwVenue.location.country
        category.text = unwVenue.categories.first?.name
        address.text = unwVenue.location.address
        city.text = unwVenue.location.city
        state.text = unwVenue.location.state
        
        guard let category = unwVenue.categories.first else {return}
        categoryIcon.sd_setImageWithURL(NSURL(string: category.iconPrefix + "bg_88" + category.iconSuffix))
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == showOnMap {
            guard let vc = segue.destinationViewController as? MapViewController else {return}
            vc.venue = venue
        }
    }
}